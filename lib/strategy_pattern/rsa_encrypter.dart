import 'package:design_patterns/strategy_pattern/encrypter_interface.dart';

class RSAEncrypter extends IEncypter {
  @override
  String decrypt(String value) {
    return value.replaceAll("RSA ", "");
  }

  @override
  String encrypt(String value) {
    return "RSA $value";
  }
}
