import 'package:design_patterns/strategy_pattern/encrypter_interface.dart';

class AESEncrypter extends IEncypter {
  @override
  String decrypt(String value) {
    return value.replaceAll("AES ", "");
  }

  @override
  String encrypt(String value) {
    return "AES $value";
  }
}
