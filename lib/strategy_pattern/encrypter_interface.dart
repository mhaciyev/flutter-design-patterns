abstract class IEncypter {
  IEncypter();

  String encrypt(String value);
  String decrypt(String value);
}
