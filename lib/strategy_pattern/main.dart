import 'package:design_patterns/strategy_pattern/aes_encrypter.dart';
import 'package:design_patterns/strategy_pattern/encrypter_interface.dart';
import 'package:design_patterns/strategy_pattern/rsa_encrypter.dart';
import 'package:flutter/material.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  TextEditingController _controller;
  IEncypter encrypter;

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController();
    encrypter = new AESEncrypter();
    // encrypter = new RSAEncrypter();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: _controller,
            decoration: InputDecoration(hintText: "Encrypter"),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [buildEncrypt(), buildDecrypt()],
          ),
        ],
      ),
    );
  }

  InkWell buildEncrypt() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text("Encrypt"),
      ),
      onTap: () {
        _controller.text = encrypter.encrypt(_controller.text);
      },
    );
  }

  InkWell buildDecrypt() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text("Decrypt"),
      ),
      onTap: () {
        _controller.text = encrypter.decrypt(_controller.text);
      },
    );
  }
}
