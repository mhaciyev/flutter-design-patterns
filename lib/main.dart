import 'strategy_pattern/main.dart';
import 'screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Design patterns',
      home: Screen(
        appBarText: "Strategy pattern",
        widget: Main(),
      ),
    );
  }
}
